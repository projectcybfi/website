const left = document.getElementById("leftSide");
const right = document.getElementById("rightSide");

const handleMove = e => {
    left.style.width = `${e.clientX / window.innerWidth * 100}%`;
}

left.onmousemove = e => handleMove(e);
right.onmouseover = e => handleMove(e);